const productsData = [
	{
		id: "64806ffcb2fb4b8c0d21a58a",
		name: "Classic Brew",
		description: "Enjoy the timeless elegance of our smooth and balanced traditional coffee.",
		price: 130,
		onOffer: true
	},
	{
		id: "64807016b2fb4b8c0d21a58c",
		name: "Creamy Caramel",
		description: "Delight in the luscious blend of caramel notes infused into our aromatic coffee.",
		price: 140,
		onOffer: true
	},
	{
		id: "64807033b2fb4b8c0d21a58e",
		name: "Vanilla Bliss",
		description: "Immerse yourself in the comforting embrace of our vanilla-infused coffee.",
		price: 140,
		onOffer: true
	},
	{
		id: "64807048b2fb4b8c0d21a590",
		name: "Decadent Irish Cream",
		description: "Unwind with the smooth and luxurious taste of Irish cream blended into our coffee.",
		price: 185,
		onOffer: true
	}
]
export default productsData;