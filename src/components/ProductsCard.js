import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductsCard({ productProp }) {
  console.log(productProp);

  // Deconstruct the product properties into their own variables
  const { _id, name, description, price } = productProp;

  return (
    <Card className="dark-card">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        {/*<Card.Subtitle>Description:</Card.Subtitle>*/}
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>
        <Button variant="warning" as={Link} to={`/products/${_id}`}>
          Details
        </Button>
      </Card.Body>
    </Card>
  );
}



