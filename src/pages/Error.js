import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "I'm currently in the process of refining and enhancing this website.",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}