import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';

export default function getAllOrders() {
  const [orderDetails, setOrderDetails] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrderDetails(data.orders);
      })
      .catch((error) => {
        console.error('Error fetching order details:', error);
      });
  }, []);

  return (
    <div className="d-flex flex-wrap justify-content-center">
      <h1 style={{ color: 'dark', textAlign: 'center', paddingBottom: '30px', width: '100%' }}>My Orders</h1>
      {/* Render the order details */}
      {orderDetails.map((order) => (
        <div key={order._id} className="card order-history mb-4" style={{ width: '100%', maxWidth: '60%', margin: '0 auto' }}>
          <Card.Body className="d-flex flex-column align-items-center">
            <Card.Title style={{ marginBottom: '0.7rem', textAlign: 'center' }}>{order.products[0].productName}</Card.Title>
            <Card.Text style={{ marginBottom: '0.1rem', textAlign: 'center' }}>Quantity: {order.products[0].quantity}</Card.Text>
            <Card.Text style={{ marginBottom: '0.7rem', textAlign: 'center' }}>Total Amount: Php {order.totalAmount}</Card.Text>
            <Card.Text style={{ marginBottom: '0.1rem', textAlign: 'center' }}>Order ID: {order._id}</Card.Text>
            <Card.Text style={{ marginBottom: '0.3rem', textAlign: 'center' }}>Purchased On: {new Date(order.purchasedOn).toLocaleString()}</Card.Text>
          </Card.Body>
        </div>
      ))}
    </div>
  );
}
