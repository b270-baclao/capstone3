import { Fragment, useEffect, useState } from 'react';
import ProductsCard from '../components/ProductsCard';

export default function Product() {
  const [products, setProducts] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(data);
        setIsLoaded(true);
      });
  }, []);

  return (
    <Fragment>
      <h1 style={{ color: 'dark', textAlign: 'center', paddingBottom: '30px' }}>Products</h1>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <div>
          {isLoaded && products.map((product, index) => (
            <div
              key={product._id}
              style={{
                animation: `fadeIn ${0.5 + (index % 2) * 0.5}s`,
                animationFillMode: 'both',
                animationDelay: `${index * 0.3}s`,
              }}
            >
              <ProductsCard productProp={product} />
            </div>
          ))}
        </div>
      </div>
      <style>
        {`
          @keyframes fadeIn {
            0% { opacity: 0; transform: translateX(${(index) => (index % 2 === 0) ? '-20px' : '20px'}); }
            100% { opacity: 1; transform: translateX(0); }
          }
        `}
      </style>
    </Fragment>
  );
}
