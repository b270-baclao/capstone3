import { useState, useEffect, useContext } from 'react';
import { Card, Button, Modal } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const navigate = useNavigate();

  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleOrder = () => {
    // const totalAmount = quantity * product.price;

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity
      })
    })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);

      if (data) {
        Swal.fire({
          title: 'Order placed!',
          icon: 'success',
          text: 'Get ready to sip, savor, and conquer the day with an extra shot of awesomeness!',
        });

          navigate('/users/details'); // Navigate to the order details page

        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, [productId]);

  if (!product) {
    return <div>Loading...</div>;
  }

  const { name, description, price } = product;

  return (
    <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '50px' }}>
      <div style={{ width: '80%', maxWidth: '500px' }}>
        <Card style={{ opacity: 0.8 }}>
          <Card.Body style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
            <div style={{ textAlign: 'left' }}>
              <Card.Title style={{ color: 'black' }}>{name}</Card.Title>
              <Card.Text style={{ color: 'black' }}>{description}</Card.Text>
              <Card.Subtitle style={{ color: 'black' }}>Price:</Card.Subtitle>
              <Card.Text style={{ color: 'black' }}>PhP {price}</Card.Text>
              <Card.Subtitle style={{ color: 'black' }}>Store Hours:</Card.Subtitle>
              <Card.Text style={{ color: 'black' }}>7:00 AM - 11:00 PM</Card.Text>
            </div>
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
              {user.id !== null ? (
                <Button variant="warning" onClick={handleShowModal}>
                  Order Now
                </Button>
              ) : (
                <Button variant="danger" as={Link} to="/login">
                  Log in to Order
                </Button>
              )}
            </div>
          </Card.Body>
        </Card>
      </div >
      <Modal show={showModal} onHide={handleCloseModal} centered>
        <Modal.Header closeButton style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)', color: 'black' }}>
          <Modal.Title>Select Quantity</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)', color: 'black' }}>
          <div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <span>Quantity:</span>
              <div>
                <Button variant="secondary" onClick={() => setQuantity(quantity - 1)} disabled={quantity === 1}>
                  -
                </Button>
                <span style={{ margin: '0 10px' }}>{quantity}</span>
                <Button variant="secondary" onClick={() => setQuantity(quantity + 1)}>
                  +
                </Button>
              </div>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '10px' }}>
              <span>Total Amount:</span>
              <span>Php {quantity * price}</span>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
          <Button variant="secondary" onClick={handleCloseModal} style={{ marginRight: '10px' }}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleOrder}>
            Confirm Order
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
